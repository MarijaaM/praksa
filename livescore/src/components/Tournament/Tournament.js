import React from "react";
import Match from "../Match/Match";
const Tournament = (props) => {
  let mecevi = null;
  //console.log(props)
  var number=Math.floor(Math.random());
  //console.log(props.tournamentName)
  if (props.mecevi) {
    mecevi = Object.values(props.mecevi).map((x) => {
     // console.log(x)
      number++;
      return <Match key={number} mec={x} />;
    });
  }
  return (
    <div>
      {props.tournamentName}
      {mecevi}
    </div>
  );
};

export default Tournament;
