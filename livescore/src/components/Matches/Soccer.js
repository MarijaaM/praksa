import React, { useEffect, useState } from "react";
import _ from "lodash";
import Category from "../category/Category";

const Soccer = (props) => {
  //console.log(props);
// const id = props.location.state.id;
 const datum = props.location.state == undefined ? "2020-03-01": props.location.state.datum;

  const [posts, setPosts] = useState([]);
/*

  const sport =
    props.match.params.id.charAt(0).toUpperCase() +
    props.match.params.id.slice(1); */
  useEffect(() => {
    fetch(
      `https://localhost:44326/api/matchbysport/2/${datum}`)
      .then((response) => response.json())
      .then((data) => setPosts(data));
  }, [datum]);


  function getPosts() {
    return posts;
  }

  _.groupByMulti = function (obj, values, context) {
    if (!values.length) return obj;
    var byFirst = _.groupBy(obj, values[0], context),
      rest = values.slice(1);
    for (var prop in byFirst) {
      byFirst[prop] = _.groupByMulti(byFirst[prop], rest, context);
    }
    return byFirst;
  };

  const allScores = _.groupByMulti(getPosts(), [
    "kategorija_NazivPrevod",
    "turnir_NazivPrevod",
  ]);

  let elementCategory = null;
  let indKey = Math.floor(Math.random());

  if (Object.entries(allScores)) {

    elementCategory = Object.keys(allScores).map((el) => {
      indKey++;

      return <Category key={indKey} categoryName={el} items={allScores[el]} />;
    });
  }

  let number2 = Math.floor(Math.random());

  return (
    <div key={number2}>
      <h1 className="naslov">Nogomet</h1>

      <div className="soccer-list2">{elementCategory}</div>
    </div>
  );
};

export default Soccer;
