import React,{useState} from "react";
import Soccer from "../Matches/Soccer";
import CalApp from '../Datepicker/CalApp';
import Sidebar from '../SideBar/SideBar';
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import MatchDetails from "../MatchDetails/MatchDetails";
import './Home.css'
import Logo from '../../Logo/logo.jpg';
import Timovi from '../SideBar/Timovi'

import {format} from 'date-fns';
const Home = (props) => {

   const [date, setDate] =  useState(format(new Date("2020-03-01"), "yyyy-MM-dd"));
   //console.log(date);
  const handleSelectDate = (date) => {
    setDate(date);
  }

  console.log(props,"home");

  /*
  let  modalHandler = (e) => {
    e.preventDefault(); //i added this to prevent the default behavior
    setShow({
     showing: true
    })
  } */
 // console.log(props);
  return (
    <div className="main">
      <BrowserRouter>
          <header className="Toolbar">
            <nav className="toolbar_navigation">
              <div className="toolbar_logo">
              <img className="logo"src={Logo}/>
              </div>
              <div className="spacer"/>
              <div className="toolbar_navigation-items">
                <ul className="uli">
                  <li className="lii">
                    <Link  to={{
                      pathname: `/${date}`,
                      state: {
                        id:2,
                        datum:date
                      }
                    }}> Nogomet </Link>
                  </li>
                  <li className="lii">
                    <Link to={{
                      pathname: '/kosarka',
                      state: {
                        id:3,
                        datum:date
                      }
                    }}>Košarka</Link>
                  </li>
                  <li className="lii">
                    <Link to={{
                      pathname: '/bejzbol',
                      state: {
                        id:4,
                        datum:date
                      }
                    }}>Bejzbol</Link>
                  </li>
                </ul>
              </div>
            </nav>
          </header>
      <div className="container">
      <div className="row">
        <div className="col-3 col-s-3">
          
          <Route path="/:id?" component={(props)=><Sidebar {...props}/>}/>

        </div>
        <div className="col-6 col-s-9">
          <CalApp date={date} handleSelectDate={handleSelectDate}/>
          <Switch>
            {}
              <Route
                path="/soccer/:matchID"
                exact
                component={(props) => <MatchDetails {...props} />}
              />
              <Route
                path="/timovi"
                component={(props) => <Timovi {...props} />}
              />
          
              <Route  path="/:date?"  component={(props)=><Soccer {...props}/>} />
              <Route  path="/"  component={(props)=><Soccer {...props}/>} />
          </Switch>
        </div>
      </div>
      </div>
      </BrowserRouter>
    </div>
  );
};

export default Home;