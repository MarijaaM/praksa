import React from "react";
import Tournament from "../Tournament/Tournament";

const Category = (props) => {
 
let all=props.items;
let all2=Object.entries(all).map(x=>{
    return <Tournament tournamentName={x[0]} mecevi={x[1]}></Tournament>
})
 
  return (
    <div className="drzava-naziv">
       {props.categoryName}
      {all2}
    </div>
  );
};

export default Category;
