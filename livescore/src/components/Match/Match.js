import React from "react";
import { Link } from "react-router-dom";

const Match = (props) => {
  let mecevi = Object.values(props).map((x) => {
    let date = new Date(x.matchDate);
    let formatdate = date.toString("hh:mm").slice(16, 21);

    return (
      <div key={x.bfMatchID} className="type1">
        <ul>
          <Link to={"/soccer/" + x.bfMatchID} className="a">
            <li className="type2">
              {formatdate} {x.domacin_NazivPrevod} - {x.team1Score}
              {"  :  "}
              {x.team2Score}
              {x.gost_NazivPrevod}
            </li>
          </Link>
        </ul>
      </div>
    );
  });

  return <div>{mecevi}</div>;
};

export default Match;
