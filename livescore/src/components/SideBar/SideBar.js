import React, { useState, useEffect} from "react";
import _ from "lodash";

import {format} from 'date-fns';
import { BrowserRouter, Route, Switch, Link } from "react-router-dom";

const CountryC = (props) => {
  const [date, setDate] =  useState(format(new Date("2020-03-01"), "yyyy-MM-dd"));
   //console.log(date);
  const handleSelectDate = (date) => {
    setDate(date);}
  const id = props.location.state == undefined ? 2: props.location.state.id;
    const [posts, setPosts] = useState([]);
   
    useEffect(() => {
      fetch(`https://localhost:44326/api/CountryClubs/${id}`)
        .then((response) => response.json())
        .then((data) => setPosts(data));
        
    }, [id]);

  const groupBy = (array, key) => {
  // Pravimo funk. groupby da bi ih grupirali
    return array.reduce((result, currentValue) => {

      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue
      );
      return result;
    }, {}); 
    
  };
 
  //Koristimo posts i gotovu nasu funkciju groupby da ih grupirali po nazivu drzave
  const drzave = groupBy(posts, "zemljaNaziv");
  const [ligaNaziv, setLigaNaziv] =useState([null]);
  
  let elementCategory = null;
  let indKey = Math.floor(Math.random());
  let el = null;

  if (Object.entries(drzave)) {

    elementCategory = Object.keys(drzave).map((el) => {
      indKey++;
      //Entrieas nam vraca sve sto se nalazi unutra. ako je isinia da postoji unos nastavi raditi
      return	el;
    });
    return (

      <div className="sidebar1">
          {elementCategory.map((value, index) => {return <li className="a" key={index}>
            <button className="bttn1" onClick= {() => show(index)}>{value}</button>
          <div id={index} style={{display: "none", paddingleft: "8px"}}>
          {drzave[value].map((value2, index2) => {return <li>
            <button className="bttn2"key={index2}>
              <Link className="linkposition" to={{
                      pathname: '/timovi',
                      state: {
                        Teamid: value2.ligaID,
                        id:id,
                        datum:date
                      }
                    }}>{value2.ligaNaziv}</Link></button></li>})}
           
          </div>
          </li>})};
         
      </div>
  
      );
  }

  // Funkcija show nam priima parametar index (random broj) koji se proslijeđuje kod mapiranja drzava iz elemenCategory
  // Uzima neki element "index" te prvjerava i mijenjannjegovo stanje iz none u block kako bi prikazali tj sakrili klikom
  function show(index){
    var x = document.getElementById(index);
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
  }

 /* function GetTournament(value) {
    /* Funkcija prima value- ime drazve. For petljom prelazimo od pocetka do kraja preko arraya objekata unutar te drzave
    U Element 2 se mapriaju elementi i njihove vrijednosti svakog objekta, zatim preko elemnt2.ligaNaziv uzimamo onu 
    vrijednst koja nama treba, u nasem slucaju naziv lige te spremamo u array Liga
  
  const Liga = [];
    for (let index = 0; index < drzave[value].length; index++) {
      let element2  = Object.fromEntries(Object.entries(drzave[value][index]).map(([key, value]) => [key, value]));
     
     Liga[index] = element2.ligaNaziv;
     
      }
 
      return Liga;
      
  } */
/* {GetTournament(value).map((value2, index2) => {return <li>
              <button className="bttn2"key={index2}>
              <Link to={{
                      pathname: '/timovi',
                      state: {
                        id:3
                      }
                    }}>{value2}</Link></button></li>})}*/
  
    
  

}
export default CountryC;