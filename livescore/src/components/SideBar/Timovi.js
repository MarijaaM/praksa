
import React, { useState, useEffect} from "react";

const Timovi = (props) => {
   const Teamid = props.location.state == undefined ? 2: props.location.state.Teamid;
    const [posts, setPosts] = useState([]);
   
    useEffect(() => {
      fetch(`https://localhost:44326/api/teams/${Teamid}`)
        .then((response) => response.json())
        .then((data) => setPosts(data));
        
    }, [Teamid]);
  let timovi = Object.values(posts).map((x) => {
    let date = new Date("2020-02-01");
    let formatdate = date.toString("hh:mm").slice(16, 21);

    return (
      
      <div>
         
            <li className="nazivTima">
              {x.timNaziv}
            </li>
            
      </div>

    );
  });

  return (
  <div>
<h4 id="h4">Klubovi</h4>
    {timovi}
  </div>
  );
};

export default Timovi;
