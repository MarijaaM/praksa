import React, { useEffect, useState } from "react";
const MatchDetails = (props) => {
  const { match } = props;
  const [details, setDetails] = useState([]);
  let idMatch = match.params.matchID;

  useEffect(() => {
    fetch("https://localhost:44326/api/sportdetails/" + idMatch)
      .then((response) => response.json())
      .then((data) => {
        setDetails(data);
      });
  }, [idMatch]);

 

  let arr = Object.values(details).map((x) => {
    if (x.goals || x.substitutions || x.periods) {
      return [].concat(x.goals, x.substitutions, x.periods);
    }
  });
  let prvoPol = Object.values(arr).map((x) => {
    if (x) {
      return x.map((y) => {
        if (y && y.goalTime && y.period == 2) {
          
          return (
            <div className="details1" key={y.idGoal}>
             {y.goalTime}' <br/>Gol: {y.playerName}{" "}
              {y.assistPlayerName1 !== "" ? (
                <div>Asistent:{y.assistPlayerName1}</div>
              ) : null}
            </div>
          );
        }else if(y&& y.substitutionTime && y.period==2){
         return ( <div className="details1">
          <li>{y.substitutionTime}'</li>
          <li>Igrač unutra: {y.playerIn}</li>{" "}
          <li>Igrač vani: {y.playerOut} </li> tim: {y.teamName}
        </div>)
        }
      });
    }
  });
  let drugoPol = Object.values(arr).map((x) => {
    if (x) {
      return x.map((y) => {
        // eslint-disable-next-line no-unused-expressions
       
        if (y && y.goalTime && y.period == 3) {
          
          return (
            <div className="details1" key={y.idGoal}>
           {y.goalTime}' <br/> Gol: {y.playerName}{" "}
              {y.assistPlayerName1 !== "" ? (
                <div>Asistent: {y.assistPlayerName1}</div>
              ) : null}
            </div>
          );
        }else if(y&& y.substitutionTime && y.period==3){
         return ( <div className="details1">
          <li>{y.substitutionTime}'</li>
          <li>Igrač unutra: {y.playerIn}</li>{" "}
          <li>Igrač vani: {y.playerOut} </li> tim: {y.teamName}
        </div>)
        }
      });
    }
  });



  let stats = Object.values(details).map((x) => {
    return Object.values(x.statistics).map((y) => {
      // console.log(y);
      return (
        <div>
          {" "}
          <li>
            {}Posjed lopte{}
          </li>
          <li>
            {} Posjed lopte {}
          </li>
          <li>
            {} Slobodni udarci {}
          </li>
          <li>
            {} Prekršaji {}
          </li>
          <li>
            {} Zaleđa {}
          </li>
          <li>
            {} Udarci u okvir {}
          </li>
          <li>
            {} Udarci van okvira {}
          </li>
          <li>
            {} Obrane vratara {}
          </li>
        </div>
      );
    });
  });

  return (
    <div>
      <div>
       
        <hr />
        <p className="poluvrijeme">Prvo poluvrijeme</p>
        {prvoPol}
        <hr />
        <p className="poluvrijeme">Drugo poluvrijeme</p>
        {drugoPol}
        <hr />
        {/*subs*/}
      </div>
    </div>
  );
};
export default MatchDetails;
