import React from 'react';

import Home from "./components/Home/Home";
import Soccer from './components/Matches/Soccer';
import Timovi from './components/SideBar/Timovi'
import {BrowserRouter} from 'react-router-dom';
function App() {
  return (
<BrowserRouter>
    <div className="App">
   
      <Home/>
    </div>
    </BrowserRouter>
  );
}

export default App;
