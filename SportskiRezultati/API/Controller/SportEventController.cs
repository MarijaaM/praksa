﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.CompilerServices;

namespace API.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class SportEventController : ControllerBase
    {
        [HttpGet("/api/routes")]
        public ActionResult Temp()
        {
            return Content("<html> 1. Ruta: /api/matchbysport/2 <br/> 2. Ruta: /api/sportdetails/875961 <br/> 3.Ruta /api/CountryClubs/2  <br/> 4. Ruta /api/SportoviZemljeLigeKluboviIgraci/2 </html>", "text/html");
        }
        [HttpGet("/api/matchbysport/{SportID}")]
       
        public List<SportEvent> MSItems(int SportID)
         {
            List<SportEvent> MSList = new List<SportEvent>();
            SportEvent MS = null;

            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spMatchBySport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdSporta", SportID);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();



                while (rd.Read())
                {
                    MS = new SportEvent();
                    MS.KeyID = Convert.ToInt32(rd["KeyID"]);
                    MS.BFMatchID = Convert.ToInt32(rd["BFMatchID"]);
                    MS.KategorijaID = Convert.ToInt32(rd["KategorijaID"]);
                    MS.Kategorija_NazivPrevod = Convert.ToString(rd["Kategorija_NazivPrevod"]);
                    MS.TurnirID = Convert.ToInt32(rd["TurnirID"]);
                    MS.Turnir_NazivPrevod = Convert.ToString(rd["Turnir_NazivPrevod"]);
                    MS.Domacin_NazivPrevod = Convert.ToString(rd["Domacin_NazivPrevod"]);
                    MS.Gost_NazivPrevod = Convert.ToString(rd["Gost_NazivPrevod"]);
                    MS.MatchDate = Convert.ToDateTime(rd["MatchDate"]);
                    MS.Sport_NazivPrevod = Convert.ToString(rd["Sport_NazivPrevod"]);
                    MS.Team1Score = Convert.ToInt32(rd["Team1Score"]);
                    MS.Team2Score = Convert.ToInt32(rd["Team2Score"]);
                    MSList.Add(MS);
                }
            }
             return MSList;
         }

        [HttpGet("/api/matchbysport/{SportID}/{datum}")]

        public List<SportEvent> MSDItems(int SportID, DateTime datum)
        {
            List<SportEvent> MSList = new List<SportEvent>();
            SportEvent MS = null;

            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spMatchBySportAndDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdSporta", SportID);
                cmd.Parameters.AddWithValue("@DateOfMatch", datum.ToString("yyyy-MM-dd"));
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();



                while (rd.Read())
                {
                    MS = new SportEvent();
                    MS.KeyID = Convert.ToInt32(rd["KeyID"]);
                    MS.BFMatchID = Convert.ToInt32(rd["BFMatchID"]);
                    MS.KategorijaID = Convert.ToInt32(rd["KategorijaID"]);
                    MS.Kategorija_NazivPrevod = Convert.ToString(rd["Kategorija_NazivPrevod"]);
                    MS.TurnirID = Convert.ToInt32(rd["TurnirID"]);
                    MS.Turnir_NazivPrevod = Convert.ToString(rd["Turnir_NazivPrevod"]);
                    MS.Domacin_NazivPrevod = Convert.ToString(rd["Domacin_NazivPrevod"]);
                    MS.Gost_NazivPrevod = Convert.ToString(rd["Gost_NazivPrevod"]);
                    MS.MatchDate = Convert.ToDateTime(rd["MatchDate"]);
                    MS.Sport_NazivPrevod = Convert.ToString(rd["Sport_NazivPrevod"]);
                    MS.Team1Score = Convert.ToInt32(rd["Team1Score"]);
                    MS.Team2Score = Convert.ToInt32(rd["Team2Score"]);
                    MSList.Add(MS);
                }
            }
            return MSList;
        }


        [HttpGet("/api/sportdetails/{MatchID}")]
        public List<SportDetails> SDItems(int MatchID)
        {
            List<SportDetails> SDList = new List<SportDetails>();
            
            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spSportDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MatchID", MatchID);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();

                List<GoalDetails> goal = new List<GoalDetails>();
                GoalDetails GD = null;
                
                while (rd.Read())
                {
                    GD = new GoalDetails();
                    GD.BFMatchID = Convert.ToInt32(rd["BFMatchID"]);
                    GD.AssistPlayerName1 = Convert.ToString(rd["AssistPlayerName1"]);
                    GD.AssistPlayerName2 = Convert.ToString(rd["AssistPlayerName2"]);
                    GD.goalTime = Convert.ToInt32(rd["goalTime"]);
                    GD.IDGoal = Convert.ToInt32(rd["IDGoal"]);
                    GD.PlayerName = Convert.ToString(rd["PlayerName"]);
                    GD.Translation = Convert.ToString(rd["GoalType"]);
                    GD.ScoringTeam = Convert.ToInt32(rd["ScoringTeam"]);
                    GD.Period = Convert.ToString(rd["GoalPeriod"]);
                    goal.Add(GD);
                }
                

                rd.NextResult();

                List<MatchPeriod> period = new List<MatchPeriod>();
                MatchPeriod SD = null;
                while (rd.Read())
                {
                    SD = new MatchPeriod();
                    SD.IDPeriodSport = Convert.ToInt32(rd["IDPeriodSport"]);
                    SD.SportID = Convert.ToInt32(rd["SportID"]);
                    SD.Team1Score = Convert.ToString(rd["Team1Score"]);
                    SD.Team2Score = Convert.ToString(rd["Team2Score"]);
                    SD.Translation = Convert.ToString(rd["Period"]);
                    period.Add(SD);
                }

                rd.NextResult();

                List<Statistics> statistics = new List<Statistics>();
                Statistics SS = null;
                while (rd.Read())
                {
                    SS = new Statistics();
                    SS.IDStatistics = Convert.ToInt32(rd["IDStatistics"]);
                    SS.StatistikaDomacin = Convert.ToString(rd["statistika_domacin"]);
                    SS.StatistikaGost = Convert.ToString(rd["statistika_gost"]);
                    SS.StatistikaNaziv = Convert.ToString(rd["statistika_naziv"]);
                    SS.IDDomacin = Convert.ToInt32(rd["DomacinID"]);
                    SS.Domacin = Convert.ToString(rd["domacin"]);
                    SS.IDGost = Convert.ToInt32(rd["GostID"]);
                    SS.Gost = Convert.ToString(rd["gost"]);
                    statistics.Add(SS);
                }
                rd.NextResult();

                List<Substitution> substitutions = new List<Substitution>();
                Substitution SB = null;
                while (rd.Read())
                {
                    SB = new Substitution();
                    SB.substitutionTime = Convert.ToInt32(rd["substitutionTime"]);
                    SB.PlayerIn = Convert.ToString(rd["PlayerInName"]);
                    SB.PlayerOut = Convert.ToString(rd["PlayerOutName"]);
                    SB.PlayerTeam = Convert.ToInt32(rd["PlayerTeam"]);
                    SB.TeamName = Convert.ToString(rd["TeamName"]);
                    SB.Period = Convert.ToString(rd["SubstitutionPeriodID"]);
                    substitutions.Add(SB);
                }
                rd.NextResult();

                List<Card> cards = new List<Card>();
                Card CD = null;
                while (rd.Read())
                {
                    CD = new Card();
                    CD.IDCard = Convert.ToInt32(rd["IDCard"]);
                    CD.CardType = Convert.ToString(rd["CardType"]);
                    CD.CardTime = Convert.ToInt32(rd["CardTime"]);
                    CD.BFPlayerID = Convert.ToInt32(rd["BFPlayerID"]);
                    CD.PlayerName = Convert.ToString(rd["PlayerName"]);
                    CD.PlayerTeam = Convert.ToInt32(rd["PlayerTeam"]);
                    CD.TeamName = Convert.ToString(rd["TeamName"]);
                    CD.CardPeriod = Convert.ToInt32(rd["CardPeriod"]);
                    cards.Add(CD);
                }
                rd.NextResult();

                List<Lineup> lineups = new List<Lineup>();
                Lineup LP = null;
                while (rd.Read())
                {
                    LP = new Lineup();
                    LP.IDLineup = Convert.ToInt32(rd["IDLineup"]);
                    LP.BFMatchID = Convert.ToInt32(rd["BFMatchID"]);
                    LP.BFPlayerID = Convert.ToInt32(rd["BFPlayerID"]);
                    LP.ShirtNumber = Convert.ToInt32(rd["ShirtNumber"]);
                    LP.PlayerName = Convert.ToString(rd["PlayerName"]);
                    LP.PlayerTeam = Convert.ToInt32(rd["PlayerTeam"]);
                    LP.TeamName = Convert.ToString(rd["TeamName"]);
                    lineups.Add(LP);
                }

                SportDetails sp = new SportDetails();
                sp.goals = goal;
                sp.periods = period;
                sp.substitutions = substitutions;
                sp.statistics = statistics;
                sp.cards = cards;
                sp.lineups = lineups;
                SDList.Add(sp);


            }
            return SDList;
        
        }

        [HttpGet("/api/CountryClubs/{SportID}")]

        public List<CountryClubs> CCItems(int SportID)
        {
            List<CountryClubs> CCList = new List<CountryClubs>();
            CountryClubs CC = null;

            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spZemljeLige",con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SportID", SportID);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();



                while (rd.Read())
                {
                    CC = new CountryClubs();
                    CC.IDKategorija = Convert.ToInt32(rd["IDKategorija"]);
                    CC.SportID = Convert.ToInt32(rd["SportID"]);
                    CC.ZemljaNaziv = Convert.ToString(rd["ZemljaNaziv"]);
                    CC.LigaID = Convert.ToInt32(rd["TurnirID"]);
                    CC.LigaNaziv = Convert.ToString(rd["LigaNaziv"]);
                    CCList.Add(CC);
                }
            }
            return CCList;
        }

        [HttpGet("/api/teams/{TurnirID}")]
        public List<Teams> TeamsItems(int TurnirID)
        {
            List<Teams> TeamsList = new List<Teams>();
            Teams teams = null;

            using(var con = db.Gdb())
            {
                SqlCommand cmd = new SqlCommand("SpTimovi", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TurnirID", TurnirID);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    teams = new Teams();
                    teams.TimID = Convert.ToInt32(rd["TimID"]);
                    teams.TimNaziv = Convert.ToString(rd["TimNaziv"]);
                    TeamsList.Add(teams);
                }
                return TeamsList;
            }
        }
        [HttpGet("/api/SportoviZemljeLigeKluboviIgraci/{IDSport}")]
        public List<SportoviZemljeLigeKluboviIgraci> SZLKIItems(int IDSport)
        {
            List<SportoviZemljeLigeKluboviIgraci> SZLKIList = new List<SportoviZemljeLigeKluboviIgraci>();
            SportoviZemljeLigeKluboviIgraci SZLKI = null;

            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spGetSportoviZemljeLigeKluboviIgraci", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdSport", IDSport);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();



                while (rd.Read())
                {
                    SZLKI = new SportoviZemljeLigeKluboviIgraci();
                    SZLKI.IDSport = Convert.ToInt32(rd["IDSport"]);
                    SZLKI.SportNaziv = Convert.ToString(rd["SportNaziv"]);
                    SZLKI.KategorijaNaziv = Convert.ToString(rd["KategorijaNaziv"]);
                    SZLKI.TurnirNaziv = Convert.ToString(rd["TurnirNaziv"]);
                    SZLKI.TimNaziv = Convert.ToString(rd["TimNaziv"]);
                    SZLKI.IgracNaziv = Convert.ToString(rd["IgracNaziv"]);
                    SZLKIList.Add(SZLKI);
                }
            }
            return SZLKIList;
        }



    }
     [HttpGet("/api/LigeiMečevi/{IDTurnir}")]
        public List<LigeiMečevi>LMItems(int IDTurnir)
        {
            List<LigeiMečevi> LMList = new List<LigeiMečevi>();
          LigeiMečevi LM = null;

            using (var con = db.Gdb())
            {

                SqlCommand cmd = new SqlCommand("spGetLigeiMečevi", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDTurnir", IDTurnir);
                con.Open();
                SqlDataReader rd = cmd.ExecuteReader();



                while (rd.Read())
                {
                    LM = new LigeiMečevi();
                   LM.IDTurnir = Convert.ToInt32(rd["IDTurnir"]);
                    LM.TurnirNaziv = Convert.ToString(rd["TurnirNaziv"]);
                    LM.MatchDate= Convert.ToDate(rd["MatchDate"]);
                    LM.Team1Score = Convert.ToInt32(rd["Team1Score"]);
                    LM.Team2Score = Convert.ToInt32(rd["Team2Score"]);
                    LM.DomacinNaziv = Convert.ToString(rd["DomacinNaziv"]);
                 LM.GostNaziv = Convert.ToString(rd["GostNaziv"]);
                 

                    LMList.Add(LM);
                }
            }
            return LMList;
        }



    }

