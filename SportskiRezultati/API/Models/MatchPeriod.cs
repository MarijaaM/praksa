﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class MatchPeriod
    {
        public int IDPeriodSport { get; set; }
        public int SportID { get; set; }
        public string Translation { get; set; }
        public string Team1Score { get; set; }
        public string Team2Score { get; set; }
    }
}
