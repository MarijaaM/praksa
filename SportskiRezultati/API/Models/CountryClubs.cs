﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
 
    public class CountryClubs
    {
        public int IDKategorija { get; set; }
        public int SportID { get; set; }
        public string ZemljaNaziv { get; set; }
        public int LigaID { get; set; }
        public string LigaNaziv { get; set; }
    }
}
