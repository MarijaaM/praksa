﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Substitution
    {
        public int substitutionTime { get; set; }
        public string PlayerIn { get; set; }
        public string PlayerOut { get; set; }
        public int PlayerTeam { get; set; }
        public string TeamName { get; set; }
        public string Period { get; set; }

    }
}
