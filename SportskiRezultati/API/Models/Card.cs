﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Card
    {
        public int IDCard { get; set; }
        public string CardType { get; set; }
        public int CardTime { get; set; }
        public int BFPlayerID { get; set; }
        public string PlayerName { get; set; }
        public int PlayerTeam { get; set; }
        public string TeamName { get; set; }
        public int CardPeriod { get; set; }
    }
}
