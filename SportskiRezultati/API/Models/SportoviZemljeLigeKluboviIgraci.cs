﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class SportoviZemljeLigeKluboviIgraci
    {

        public int IDSport { get; set; }
        public string SportNaziv { get; set; }

        public string KategorijaNaziv { get; set; }
        public string TurnirNaziv { get; set; }
        public string TimNaziv { get; set; }
        public string IgracNaziv { get; set; }

    }
}


