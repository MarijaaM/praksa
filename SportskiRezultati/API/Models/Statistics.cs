﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Statistics
    {
        public int IDStatistics { get; set; }
        public string StatistikaDomacin { get; set; }
        public string StatistikaGost { get; set; }
        public string StatistikaNaziv { get; set; }
        public int IDDomacin { get; set; }
        public string Domacin { get; set; }
        public int IDGost { get; set; }
        public string Gost { get; set; }
    }
}
