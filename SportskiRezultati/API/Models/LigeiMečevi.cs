﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class LigeiMečevi
    {

        public int IDTurnir { get; set; }
        public string TurnirNaziv { get; set; }

        public string MatchDate { get; set; }
        public string Team1Score{ get; set; }
        public string Team2Score { get; set; }
        public string DomacinNaziv { get; set; }
        public string GostNaziv { get; set; }


    }
}
