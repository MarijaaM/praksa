﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class GoalDetails
    {
        public int IDGoal { get; set; } 
        public int BFMatchID { get; set; }
        public int goalTime { get; set; }
        public string PlayerName { get; set; } 
        public string AssistPlayerName1 { get; set; }
        public string AssistPlayerName2 { get; set; }
        public string Translation { get; set; }
        public int ScoringTeam { get; set; }
        public string Period { get; set; }
    }
}
