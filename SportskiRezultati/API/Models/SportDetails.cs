﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class SportDetails
    {
        public List<GoalDetails> goals { get; set; }
        public List<MatchPeriod> periods { get; set; }
        public List<Substitution> substitutions { get; set; }
        public List<Statistics> statistics { get; set; }
        public List<Card> cards { get; set; }
        public List<Lineup> lineups { get; set; }

    }
}
