﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Lineup
    {
        public int IDLineup { get; set; }
        public int BFMatchID { get; set; }
        public int BFPlayerID { get; set; }
        public int ShirtNumber { get; set; }
        public string PlayerName { get; set; }
        public int PlayerTeam { get; set; }
        public string TeamName { get; set; }
    }
}
