﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class SportEvent
    {
        public int KeyID { get; set; }
        public int BFMatchID { get; set; }
        public int KategorijaID { get; set; }
        public string Kategorija_NazivPrevod { get; set; }
        public int TurnirID { get; set; }
        public string Turnir_NazivPrevod { get; set; }
        public string Domacin_NazivPrevod { get; set; }
        public string Gost_NazivPrevod { get; set; }
        public DateTime MatchDate { get; set; }
        public string Sport_NazivPrevod { get; set; }
        public int Team1Score { get; set; }
        public int Team2Score { get; set; }

    }
}
